import React, { Component } from "react";
import Header from "./Header";
import SearchPanel from "./SearchPanel";
import QuoteCardContainer from "./QuoteCardContainer";
import "./App.css";

const baseUrl = "http://morty.mockable.io/quotes?";

function FilterOptions(options) {
  this.lender = options ? options.lenders : [];
}

const isObject = candidate => {
  return typeof candidate === "object" && candidate !== null;
};

class App extends Component {
  state = {
    quotes: [],
    filterOptions: new FilterOptions(),
    sortOptions: ["lender"],
    sortOrder: null,
    filtered: false,
    message: null
  };
  submit = async e => {
    e.preventDefault();
    const loanAmount = e.target.elements.loanAmount.value;
    if (loanAmount) {
      const loanParam = `loan_amount=${loanAmount}`;
      try {
        const res = await fetch(baseUrl + loanParam);
        const data = await res.json();
        if (data.length) {
          this.resetFilterOptions();
          this.setFilter(data);
          this.setState({ quotes: data, message: null });
        } else {
          this.setState({ message: "no quotes found" });
        }
      } catch (error) {
          this.setState({ message: "there was a problem fetching quotes" })
      }
    }
  };
  resetFilterOptions = () => {
    this.setState({
      filterOptions: new FilterOptions()
    });
  };
  setFilter = quotes => {
    // extract and set sort and filter options from quotes
    // just lenders for now
    const lenderTracker = {};
    quotes.forEach(q => {
      lenderTracker[q.lender.name] = true;
    });
    const lenders = Object.keys(lenderTracker);
    this.setState({
      filterOptions: new FilterOptions({ lenders })
    });
  };
  resetFilter = () => {
    this.setState({
      quotes: this.state.quotes.map(q => ({ ...q, hide: false })),
      filtered: false
    });
  };
  applySort = sortValue => {
    this.setState({
      quotes: this.state.quotes.sort((a, b) => {
        const isNested = isObject(a[sortValue]);
        if (isNested) {
          // assume obj with name and default lexographic
          if (this.state.sortOrder === "asc") {
            this.setState({ sortOrder: "des" });
            return a[sortValue].name.localeCompare(b[sortValue].name);
          } else {
            this.setState({ sortOrder: "asc" });
            return b[sortValue].name.localeCompare(a[sortValue].name);
          }
        } else {
          // numerical sorts etc.
        }
      })
    });
  };
  applyFilters = filters => {
    this.setState({
      quotes: this.state.quotes.map(q => {
        q.hide = false;
        for (let filter in filters) {
          // assume obj with name
          // just string filters for now
          let compareVal = isObject(q[filter]) ? q[filter].name : q[filter];
          if (!filters[filter].includes(compareVal)) {
            q.hide = true;
            break;
          }
        }
        return q;
      }),
      filtered: true
    });
  };
  render() {
    return (
      <div className="app">
        <Header />
        <SearchPanel submit={this.submit} message={this.state.message} />
        {this.state.quotes.length > 0 && (
          <QuoteCardContainer
            quotes={this.state.quotes}
            setFilters={this.applyFilters}
            filterOptions={this.state.filterOptions}
            sortOptions={this.state.sortOptions}
            applySort={this.applySort}
            filtered={this.state.filtered}
            resetFilter={this.resetFilter}
          />
        )}
      </div>
    );
  }
}

export default App;
