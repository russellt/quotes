import React, { Component } from "react";
import "./Dropdown.css";

class Dropdown extends Component {
	state = {
		expanded: false
	};
	toggle = () => {
		this.setState({ expanded: !this.state.expanded });
	};
	render() {
		const { title, options, handleOptions, categories } = this.props;
		return (
			<div className="dropdown">
				<div className="dropdown-title" onClick={this.toggle}>
					{title} <span className="down-carot">&#x25BC;</span>
				</div>
				{this.state.expanded &&
					categories &&
					Object.keys(options).map(category => (
						<div key={category}>
							<span className="filter-option-type">{category}</span>
							{options[category].map(o => (
								<div
									key={o}
									className="filter-option"
									onClick={handleOptions.bind(null, { [category]: o })}
								>
									{o}
								</div>
							))}
						</div>
					))}
				{this.state.expanded &&
					!categories &&
					options.map(o => (
						<div
							className="filter-option"
							key={o}
							onClick={handleOptions.bind(null, o)}
						>
							{o}
						</div>
					))}
			</div>
		);
	}
}

export default Dropdown;
