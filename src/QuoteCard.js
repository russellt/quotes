import React from 'react';
import './QuoteCard.css';

const QuoteCard = ({ quote }) => (
  <div className="quote-card">
  	<div>  		
	  	<div className="interest-rate">{quote.interest_rate}%</div>
	  	<div className="loan-term">{quote.loan_term} Year {quote.rate_type}</div>
	  	<div className="lender">{quote.lender.name}</div>
  	</div>
  	<div className="monthly-payment">${quote.monthly_payment}</div>
  </div>        
)

export default QuoteCard;
