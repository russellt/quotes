import React from "react";
import QuoteCard from "./QuoteCard";
import Dropdown from "./Dropdown";
import "./QuoteCardContainer.css";

const QuoteCardContainer = ({
  quotes,
  setFilters,
  filterOptions,
  sortOptions,
  applySort,
  filtered,
  resetFilter
}) => (
  <div className="quote-card-container">
    {filtered && (
      <span className="reset" onClick={resetFilter}>
        reset filters
      </span>
    )}
    <div className="quote-sort-filter">
      <Dropdown
        title="Filter By"
        categories="true"
        handleOptions={setFilters}
        options={filterOptions}
      />
      <Dropdown
        title="Sort By"
        handleOptions={applySort}
        options={sortOptions}
      />
    </div>
    {quotes.reduce((acc, q, i) => {
      if (!q.hide) {
        acc.push(
          <QuoteCard
            show={q.show}
            quote={{ ...q, rate_type: "fixed" }}
            key={i}
          />
        );
      }
      return acc;
    }, [])}
  </div>
);

export default QuoteCardContainer;
